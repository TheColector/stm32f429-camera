# STM32F429 CAMERA

Project's goal is to make functional, yet simple camera device that can use STM32F429 + camera module + board's embedded TFT LCD to display pictures that have been taken. If everything works well, further improvements are planned.

Project's hardware:
- Camera module - OV7670
- Microcontroller - STM32F429
- Development board - STM32F429 DISCOVERY


Working principle:
Camera module outputs image on it's parrarel interface, then microcontroller's peripheral called DCMI captures these bits with every synchronization clock cycle that the camera also outputs. Such a package of bits is loaded to the dedicated frame buffer with the use of peripheral to memmory DMA transfers. If the frame is completed, it can be send to the LCD display. The tricky part is to get everything working in term of timing and ordering of bits, as they might differ according to the parameters that had been set during inicialization. 

As of the February 2021, there seems to be an issue with the module itself, because the displayed image is noisy and doesn't seem to react properly to the change of the scenery that is in front of the lens.
