/*
 * camera.h
 *
 *  Created on: Dec 19, 2020
 *      Author: MiM
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include <stm32f429xx.h>
#include "sccb.h"
#include "gpio.h"

#define FRAME_WIDTH		(240U)
#define FRAME_HEIGHT	(320U)
#define OV7670_REG_NUM	(123U)

extern volatile uint16_t frameBuffer[FRAME_HEIGHT*FRAME_WIDTH];

void cameraInit(void);
void MCO1InitClk(void);


#endif /* CAMERA_H_ */
