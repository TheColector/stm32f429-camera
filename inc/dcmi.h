/*
 * dcmi.h
 *
 *  Created on: Nov 14, 2020
 *      Author: MiM
 */

#ifndef DCMI_H_
#define DCMI_H_

#include "stm32f429xx.h"
#include "gpio.h"
#include "camera.h"
#include <stdbool.h>



void dcmiInit(void);
void dcmiFrameCapture(void);
bool dcmiIsReady(void);

#endif /* DCMI_H_ */
