/*
 * sccb.h
 *
 *  Created on: Nov 26, 2020
 *      Author: MiM
 */

#ifndef SCCB_H_
#define SCCB_H_

#include <stdint.h>
#include <stddef.h>

#define SCCB_ID_WR	(0x42U)
#define SCCB_ID_RD	(0x43U)

typedef enum
{
 SCCB_RESULT_ERROR = 0,
 SCCB_RESULT_OK,
 SCCB_RESULT_OUT_OF_RANGE_FREQ,

} SccbResult;

/*	@brief Initialize hardware: Timer, data and clock pins for transferring and receiving data
 * 	@param clockFrequency frequency of the sccb protocol
 * 	@return Status flag of operation
 */
SccbResult sccbInit(uint32_t clockFrequency);

/*	@brief Start process of data transmission to the specific device
 * 	@param idAddress write address of a device
 * 	@param subAddress address of the register one wants to write data to
 * 	@param data data to send
 * 	@return Status flag of operation
 */
SccbResult sccbSend(uint8_t idAddress, uint8_t subAddress, uint8_t data);

/*	@brief Start process of receiving data
 * 	@param idAddress read address of a device
 * 	@param subAddress address of the register one wants to read data from
 * 	@param data data received
 * 	@return Status flag of operation
 */
SccbResult sccbReceive(uint8_t idAddress, uint8_t subAddress, uint8_t* data);

#endif /* SCCB_H_ */
