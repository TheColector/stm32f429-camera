/*
 * spi.h
 *
 *  Created on: Dec 20, 2020
 *      Author: MiM
 */

#ifndef SPI_H_
#define SPI_H_

#include "stm32f4xx.h"
#include <stm32f4xx.h>
#include <stdbool.h>
#include "gpio.h"
#include "camera.h"

void spiInit(void);
void spiLcd8Bit(void);
void spiLcd16Bit(void);
void spiSend(uint16_t data);
void spiSendBuffer(uint8_t* data, uint8_t length);


#endif /* SPI_H_ */
