/*
 * clock_setup.h
 *
 *  Created on: Nov 4, 2020
 *      Author: MiM
 */

#ifndef INC_SYSTEM_INIT_H_
#define INC_SYSTEM_INIT_H_

#define SYSTEM_CLK_HZ   (160000000U)
#define AHB1_CLK_HZ     (SYSTEM_CLK_HZ/4)
#define AHB1_TIM_CLK_HZ (AHB1_CLK_HZ*2)
#define APB1_CLK_HZ   	(SYSTEM_CLK_HZ/4)
#define APB1_TIM_CLK_HZ (APB1_CLK_HZ*2)
#define APB2_CLK_HZ   	(SYSTEM_CLK_HZ/2)
#define APB2_TIM_CLK_HZ (APB2_CLK_HZ*2)
#define CORE_CLK_HZ		(SYSTEM_CLK_HZ)


void setClock(void);

extern uint32_t systemClock;


#endif /* INC_SYSTEM_INIT_H_ */
