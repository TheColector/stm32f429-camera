/*
 * tft_lcd.h
 *
 *  Created on: Nov 14, 2020
 *      Author: MiM
 */

#ifndef TFT_LCD_H_
#define TFT_LCD_H_

#include <stdlib.h>
#include <stdbool.h>
#include <stm32f4xx.h>
#include "stm32f429xx.h"
#include "spi.h"
#include "gpio.h"
#include "timer.h"
#include "camera.h"

typedef uint16_t Color;
typedef uint32_t Int;

typedef enum
{
 LCD_ORIENTATION_LANDSCAPE = 0,
 LCD_ORIENTATION_LANDSCAPE_REV,
 LCD_ORIENTATION_PORTRAIT,
 LCD_ORIENTATION_PORTRAIT_REV
} LcdOrientation;

//************************************* LCD SIZES BEGIN ***************************************************************

#define LCD_HEIGHT 		(320U)
#define LCD_WIDTH 		(240U)
#define LCD_PIX_SIZE	(LCD_HEIGHT*LCD_WIDTH)

//************************************* LCD SIZES END *****************************************************************


//************************************* LCD COLORS BEGIN **************************************************************

#define WHITE 0xFFFF
#define BLACK 0x0000
#define GRAY  0x7BEF
#define RED   0xF800
#define GREEN 0x07E0
#define BLUE  0x001F
#define YELLOW  0xFFE0
#define ORANGE  0xFCA0
#define BROWN   0x8200
#define VIOLET  0x9199
#define SILVER  0xA510
#define GOLD  0xA508
#define PURPLE  0x780F

//************************************* LCD COLORS END ****************************************************************


//************************************* LCD INIT BEGIN ****************************************************************

void lcdInit(void);
void lcdUpdate(void);

//************************************* LCD INIT END ******************************************************************


//************************************* LCD DRAWING FUNCTIONS BEGIN ***************************************************
void lcdSetOrientation(LcdOrientation orientation);
void preFill(void);
void setDrawingWindow(Int xBegin, Int xEnd, Int yBegin, Int yEnd);
void lcdFill(Color color);
void lcdDrawImage(const uint16_t image[LCD_PIX_SIZE]);
void lcdDrawPixel(Int x, Int y, Color color);
void lcdDrawHLine(Int x1, Int x2, Int y, Color color);
void lcdDrawVLine(Int x, Int y1, Int y2, Color color);
void lcdDrawLine(Int x1, Int x2, Int y1, Int y2, Color color);
void lcdDrawRectangle(Int x1, Int x2, Int y1, Int y2, Color color);
void lcdDrawCircle(Int x, Int y, Int r, Color color);

//************************************* LCD DRAWING FUNCTIONS END *****************************************************

#endif /* TFT_LCD_H_ */
