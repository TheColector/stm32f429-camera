/*
 * timer.h
 *
 *  Created on: Nov 5, 2020
 *      Author: MiM
 */

#ifndef INC_TIMER_H_
#define INC_TIMER_H_

extern volatile uint32_t currentTimeInUs;

void timerUsInit(void);
void timerUsStart(void);
void timerUsStop(void);
void resetUsTime(void);
uint32_t getCurrentTimeUs(void);

void delayUs(uint32_t us);

#endif /* INC_TIMER_H_ */
