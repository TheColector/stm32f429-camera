/*
 * dcmi.c
 *
 *  Created on: Nov 14, 2020
 *      Author: MiM
 */

#include "dcmi.h"

#define HSYNC_PORT	GPIOA
#define HSYNC_PIN	4
#define VSYNC_PORT	GPIOG
#define VSYNC_PIN	9
#define PCLK_PORT	GPIOA
#define PCLK_PIN	6
#define D0_PORT		GPIOC
#define D0_PIN		6
#define D1_PORT		GPIOA
#define D1_PIN		10
#define D2_PORT		GPIOC
#define D2_PIN		8
#define D3_PORT		GPIOC
#define D3_PIN		9
#define D4_PORT		GPIOC
#define D4_PIN		11
#define D5_PORT		GPIOB
#define D5_PIN		6
#define D6_PORT		GPIOB
#define D6_PIN		8
#define D7_PORT		GPIOB
#define D7_PIN		9

static volatile bool ready;


void dcmiInit(void)
{
 /*	DCMI clock enable */
 RCC->AHB2ENR |= RCC_AHB2ENR_DCMIEN;

 /*	GPIO setup */
 gpioPinConfiguration(HSYNC_PORT, HSYNC_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
 gpioPinAlternateFunctionConfiguration(HSYNC_PORT, HSYNC_PIN, 13);
 gpioPinConfiguration(VSYNC_PORT, VSYNC_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(VSYNC_PORT, VSYNC_PIN, 13);
 gpioPinConfiguration(PCLK_PORT, PCLK_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(PCLK_PORT, PCLK_PIN, 13);
 gpioPinConfiguration(D0_PORT, D0_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D0_PORT, D0_PIN, 13);
 gpioPinConfiguration(D1_PORT, D1_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D1_PORT, D1_PIN, 13);
 gpioPinConfiguration(D2_PORT, D2_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D2_PORT, D2_PIN, 13);
 gpioPinConfiguration(D3_PORT, D3_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D3_PORT, D3_PIN, 13);
 gpioPinConfiguration(D4_PORT, D4_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D4_PORT, D4_PIN, 13);
 gpioPinConfiguration(D5_PORT, D5_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D5_PORT, D5_PIN, 13);
 gpioPinConfiguration(D6_PORT, D6_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D6_PORT, D6_PIN, 13);
 gpioPinConfiguration(D7_PORT, D7_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
 gpioPinAlternateFunctionConfiguration(D7_PORT, D7_PIN, 13);

 /* DCMI config
  *
  * FCRC[1:0] = 00 -> All frames are captured
  * EDM[1:0] = 00 -> Interface captures 8-bit data on every pixel clock
  * ESS = 0 -> Hardware synchronization mode
  * VSPOL = 1 -> active high
  * HSPOL = 0 -> active low
  * PCKPOL = 1 -> MISing edge active
  * CM = 0 -> Continuous grab mode
  * CM = 1 -> Snapshot mode
  *
 */

// DCMI->CR &= ~(DCMI_CR_EDM_0 | DCMI_CR_EDM_1 | DCMI_CR_FCRC_0 | DCMI_CR_FCRC_1  | DCMI_CR_HSPOL | DCMI_CR_ESS | DCMI_CR_CM);
// DCMI->CR |= (DCMI_CR_PCKPOL | DCMI_CR_VSPOL);
 DCMI->CR = DCMI_CR_PCKPOL | DCMI_CR_VSPOL;
 DCMI->IER = DCMI_IER_VSYNC_IE | DCMI_IER_FRAME_IE | DCMI_IER_OVF_IE | DCMI_IER_LINE_IE;

 NVIC_SetPriority(DCMI_IRQn, 3);
 NVIC_EnableIRQ(DCMI_IRQn);

 DCMI->CR |= DCMI_CR_ENABLE;

 // clock setup for DMA2
 RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;

 /*
  * CHSEL[2:0] = 001 -> DMA2 channel 1 -> DCMI request
  *	DIR[1:0]: = 00 -> PERIPHERAL TO MEMORY transfer
  *	PL[1:0] = 11 -> very high priority
  *	PFCTRL = 0 -> DMA as flow controller
  *	MSIZE[1:0] = 01 -> memory size as half-word
  *	PSIZE[1:0] = 10 -> peripheral size as word
  *	MINC = 1 -> Memory increment mode
  * CIRC = 1 -> Circular mode
  * TCIE = 1 -> Transfer complete interrupt enable
  * DMDIS = 1 -> Direct mode disabled
  * FTH[1:0] = 11 -> full FIFO threshold selection
  * MBURST[1:0] = 00 -> single transfer
  * PBURST[1:0] = 00 -> single transfer
 */

 DMA2_Stream7->CR = DMA_SxCR_CHSEL_0 | DMA_SxCR_PL | DMA_SxCR_MSIZE_1 | DMA_SxCR_PSIZE_1 | DMA_SxCR_MINC | DMA_SxCR_TCIE;

 NVIC_SetPriority(DMA2_Stream7_IRQn, 2);
 NVIC_EnableIRQ(DMA2_Stream7_IRQn);
}


void dcmiFrameCapture(void)
{
 ready = false;
 DMA2_Stream7->PAR = (uint32_t) &DCMI->DR;
 DMA2_Stream7->M0AR = (uint32_t) frameBuffer;
 DMA2_Stream7->NDTR = FRAME_HEIGHT*FRAME_WIDTH/2;

 DMA2_Stream7->CR = DMA_SxCR_CHSEL_0 | DMA_SxCR_PL | DMA_SxCR_MSIZE_1 | DMA_SxCR_PSIZE_1 | DMA_SxCR_MINC | DMA_SxCR_TCIE;
 DMA2_Stream7->CR |= DMA_SxCR_EN;

 DCMI->CR |= DCMI_CR_CAPTURE;
}

bool dcmiIsReady(void)
{
 return ready;
}

void DMA2_Stream7_IRQHandler(void)
{
 if(DMA2->HISR & DMA_HISR_TCIF7)
 {
  DMA2->HIFCR |= DMA_HIFCR_CTCIF7;
  DCMI->CR &= ~DCMI_CR_CAPTURE;
  DMA2_Stream7->CR &= ~DMA_SxCR_EN;
  ready = true;
  gpioBitToggle(GPIOG, 14);
 }
}

void DCMI_IRQHandler(void)
{
 if(DCMI->MISR & DCMI_MISR_VSYNC_MIS)
 {
  DCMI->ICR |= DCMI_ICR_VSYNC_ISC;
  gpioBitToggle(GPIOG, 13);
//  dcmiVSyncIT = true;
 }
 if(DCMI->MISR & DCMI_MISR_LINE_MIS)
 {
  DCMI->ICR |= DCMI_ICR_LINE_ISC;

//  dcmiFrameIT = true;
 }
 if(DCMI->MISR & DCMI_MISR_FRAME_MIS)
 {
  DCMI->ICR |= DCMI_ICR_FRAME_ISC;
//  dcmiFrameIT = true;
 }
 if(DCMI->MISR & DCMI_MISR_OVF_MIS)
 {
  DCMI->ICR |= DCMI_ICR_OVF_ISC;
//  dcmiOvfErrorIT = true;
 }
}
