#include "stm32f429xx.h"
#include "system_init.h"
#include "timer.h"
#include "gpio.h"
#include "spi.h"
#include "tft_lcd.h"
#include "camera.h"
#include "dcmi.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>

volatile bool dcmiOvfErrorIT = false;
volatile bool dcmiOvrErrorIT = false;
volatile bool dcmiVSyncIT = false;
volatile bool dcmiFrameIT = false;
volatile bool frameReady = false;

int main(void)
{
 NVIC_SetPriorityGrouping(4);

 //  Clock and timer enable
 gpioInitialize();

 // GPIO setup
 gpioPinConfiguration(GPIOG, 14, GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);  // GPIOG PIN 14 DIODE for debug purposes
 gpioPinConfiguration(GPIOG, 13, GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);  // GPIOG PIN 13 DIODE for debug purposes

 gpioPinConfiguration(GPIOA, 8,
	                  GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);  //MCO1 outputs clock for camera
 gpioPinAlternateFunctionConfiguration(GPIOA, 8, 0);

 cameraInit();
 lcdInit();
 dcmiInit();
 dcmiFrameCapture();

 while(1)
 {
//  lcdFill(RED);
//  lcdFill(GREEN);
//  lcdFill(BLUE);

  if(dcmiIsReady())
  {
   lcdDrawImage((uint16_t*)frameBuffer);
   dcmiFrameCapture();
  }
 }
}
