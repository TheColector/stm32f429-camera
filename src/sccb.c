/*
 * sccb.c
 *
 *  Created on: Nov 26, 2020
 *      Author: MiM
 */
#include "sccb.h"
#include "stm32f4xx.h"
#include "gpio.h"
#include "system_init.h"
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "timer.h"

#define SCCB_FREQ       	(100000U)
#define SCCB_FREQ_MIN   	(AHB1_TIM_CLK_HZ/(UINT16_MAX+1)/4U)
#define SCCB_FREQ_MAX		(100000U)
#define SCCB_TIMER      	(TIM7)
#define SCCB_CLK_GPIO   	(GPIOG)
#define SCCB_CLK_PIN    	(0)
#define SCCB_DATA_GPIO  	(GPIOG)
#define SCCB_DATA_PIN   	(1)
#define IS_HIGH         	(true)
#define IS_LOW          	(false)
#define BITS_SEND_WR		(9U)
#define BITS_RECEIVE_RD 	(9U)
#define TICKS_PER_BIT		(4U)

typedef enum
{
 DIR_OUT = 0,
 DIR_IN
} Dir;

typedef enum
{
 STATE_START_WRITE = 0,
 STATE_START_READ,
 STATE_ID_ADDRESS_WRITE,
 STATE_ID_ADDRESS_READ_1,
 STATE_ID_ADDRESS_READ_2,
 STATE_SUB_ADDRES_WRITE,
 STATE_SUB_ADDRES_READ,
 STATE_READ_DATA,
 STATE_WRITE_DATA,
 STATE_STOP,
 STATE_FINISHED
} State;

typedef struct
{
 State state;
 uint8_t index;
 uint8_t indexLength;
} StateMachineData;

static volatile StateMachineData stateMachineData;
static Dir currentDataPinType = DIR_IN;
static Dir expectedDataPinType = DIR_IN;
static bool clkState = IS_HIGH;
static bool readSequence = false;
static State nextState;
static uint8_t nextIndexLength;

volatile uint8_t idAddress_v = 0;
volatile uint8_t subAddress_v = 0;
volatile uint8_t data_v = 0;
volatile uint8_t dataReturn = 0;
volatile uint16_t cnt = 0;

static inline void timerStart(TIM_TypeDef* const __restrict__ tim)
{
 tim->CR1 |= TIM_CR1_CEN;
}

static inline void timerStop(TIM_TypeDef* const __restrict__ tim)
{
 tim->CR1 &= ~TIM_CR1_CEN;
}

static inline void timerReset(TIM_TypeDef* const __restrict__ tim)
{
 tim->CNT = 0;
// tim->EGR |= TIM_EGR_UG;
}

static inline void clkEdgeRising(void)
{
 gpioBitSet(SCCB_CLK_GPIO, SCCB_CLK_PIN);
}

static inline void clkEdgeFalling(void)
{
 gpioBitReset(SCCB_CLK_GPIO, SCCB_CLK_PIN);
}

static inline void dataEdgeRising(void)
{
 gpioBitSet(SCCB_DATA_GPIO, SCCB_DATA_PIN);
}

static inline void dataEdgeFalling(void)
{
 gpioBitReset(SCCB_DATA_GPIO, SCCB_DATA_PIN);
}

static inline void debugDiodeSet(void)
{
 gpioBitSet(GPIOG, 13);
}

static inline void debugDiodeReset(void)
{
 gpioBitReset(GPIOG, 13);
}

static inline void dataPinDirection(Dir dir)
{
 if(dir == DIR_OUT)
 {
  gpioPinConfiguration(SCCB_DATA_GPIO, SCCB_DATA_PIN,
	                   GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);
  currentDataPinType = DIR_OUT;
 }
 else
 {
  gpioPinConfiguration(SCCB_DATA_GPIO, SCCB_DATA_PIN,
	                   GPIO_MODE_INPUT_FLOATING);
  currentDataPinType = DIR_IN;
 }
}

static void setDataPinType(Dir dataPinCurrentType, Dir dataPinExpectedType)
{
 if(dataPinCurrentType != dataPinExpectedType)
 {
  if(dataPinExpectedType == DIR_OUT)
  {
   dataPinDirection(DIR_OUT);
  }
  else if(dataPinExpectedType == DIR_IN)
  {
   dataPinDirection(DIR_IN);
  }
 }
}

static bool dataPinRead(void)
{
 return GPIO_TRUE == gpioBitRead(SCCB_DATA_GPIO, SCCB_DATA_PIN);
}

static void changeClkState(bool ClkState)
{
 if(clkState == IS_HIGH)
 {
  clkEdgeFalling();
  clkState = IS_LOW;
 }
 else
 {
  clkEdgeRising();
  clkState = IS_HIGH;
 }
}

static void dataWrite(uint8_t data, uint8_t indexer)
{
 if((data << indexer) & (0x80))
 {
  dataEdgeRising();
 }
 else
 {
  dataEdgeFalling();
 }
}

static void readData(uint8_t* data, uint8_t indexer)
{
 if(dataPinRead())
 {
  *data |= (1 << (7 - indexer));
 }
 else
 {
  *data &= ~(1 << (7 - indexer));
 }
}

static void startSequence(void)
{
 if((stateMachineData.indexLength) > stateMachineData.index)
 {
  if(stateMachineData.index == 1)
  {
   assert(clkState == IS_HIGH);
   dataEdgeFalling();
  }

  stateMachineData.index++;
 }
 else
 {
  assert(clkState == IS_HIGH);
  changeClkState(clkState);
  stateMachineData.index = 0;
  stateMachineData.indexLength = nextIndexLength;
  stateMachineData.state = nextState;
  debugDiodeReset();
  cnt = 0;
 }
}

static void stopSequence(void)
{
 if(stateMachineData.indexLength > stateMachineData.index)
 {
  if(stateMachineData.index == 0)
  {
   assert(clkState == IS_LOW);
   dataEdgeFalling();
  }
  else if(stateMachineData.index == 1)
  {
   changeClkState(clkState);
  }
  else if(stateMachineData.index == 2)
  {
   dataEdgeRising();
  }

  stateMachineData.index++;
 }
 else
 {
  assert(clkState == IS_HIGH);
  stateMachineData.indexLength = nextIndexLength;
  stateMachineData.index = 0;
  stateMachineData.state = nextState;
  expectedDataPinType = DIR_OUT;
  setDataPinType(currentDataPinType, expectedDataPinType);
  debugDiodeReset();
  cnt = 0;
 }
}


SccbResult sccbInit(uint32_t clockFrequency)
{
 if((clockFrequency < SCCB_FREQ_MIN) || (clockFrequency > SCCB_FREQ_MAX))
 {
  return SCCB_RESULT_OUT_OF_RANGE_FREQ;
 }

 gpioPinConfiguration(SCCB_CLK_GPIO, SCCB_CLK_PIN,
	                  GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);
 gpioPinConfiguration(SCCB_DATA_GPIO, SCCB_DATA_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);

 clkEdgeRising();

 RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;

 SCCB_TIMER->PSC = 1 - 1;
 SCCB_TIMER->ARR = (AHB1_TIM_CLK_HZ / clockFrequency / 4) - 1;

 SCCB_TIMER->CR1 = TIM_CR1_URS;
 SCCB_TIMER->DIER = TIM_DIER_UIE;

 //interrupt enable
 NVIC_EnableIRQ(TIM7_IRQn);

 stateMachineData.index = 0;
 stateMachineData.indexLength = 0;
 stateMachineData.state = STATE_STOP;

 return SCCB_RESULT_OK;
}

SccbResult sccbSend(uint8_t idAddress, uint8_t subAddress, uint8_t data)
{
 stateMachineData.state = STATE_START_WRITE;
 stateMachineData.indexLength = 3;
// nextIndexLength = 3;
// nextState = STATE_START_WRITE;

 idAddress_v = idAddress;
 subAddress_v = subAddress;
 data_v = data;

 timerStart(SCCB_TIMER);

 while(stateMachineData.state != STATE_FINISHED);
 timerStop(SCCB_TIMER);

 if(1)
 {
  // Return possible error
 }

 return SCCB_RESULT_OK;
}

SccbResult sccbReceive(uint8_t idAddress, uint8_t subAddress, uint8_t* dataMain)
{
 stateMachineData.index = 0;
 stateMachineData.indexLength = 3;
 stateMachineData.state = STATE_START_READ;

 idAddress_v = idAddress;
 subAddress_v = subAddress;

 timerStart(SCCB_TIMER);

 while(stateMachineData.state != STATE_FINISHED);

 *dataMain = dataReturn;
 uint8_t tmp11 = *dataMain;
 uint8_t tmp22 = dataReturn;

 timerStop(SCCB_TIMER);

 if(1)
 {
  // Return possible error
 }

 return SCCB_RESULT_OK;
}

void TIM7_IRQHandler(void)
{
 static uint8_t writeData;

 if(SCCB_TIMER->SR & TIM_SR_UIF)
 {
  SCCB_TIMER->SR &= ~TIM_SR_UIF;
 }

 switch(stateMachineData.state)
 {
  case STATE_START_WRITE:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	debugDiodeSet();
	nextIndexLength = (TICKS_PER_BIT * BITS_SEND_WR) - 1;
	nextState = STATE_ID_ADDRESS_WRITE;
	expectedDataPinType = DIR_OUT;
	setDataPinType(currentDataPinType, expectedDataPinType);
   }
  }
   break;
  case STATE_START_READ:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	cnt = 0;
	debugDiodeSet();
	stateMachineData.index = 0;
	nextIndexLength = (TICKS_PER_BIT * BITS_SEND_WR) - 1;
	nextState = (readSequence) ? STATE_ID_ADDRESS_READ_2 : STATE_ID_ADDRESS_READ_1;
	expectedDataPinType = DIR_OUT;
	setDataPinType(currentDataPinType, expectedDataPinType);
   }
  }
   break;
  case STATE_ID_ADDRESS_WRITE:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	cnt = 0;
	debugDiodeSet();
	stateMachineData.index = 0;
	nextIndexLength = (TICKS_PER_BIT * BITS_SEND_WR) - 1;
	nextState = STATE_SUB_ADDRES_WRITE;
	expectedDataPinType = DIR_OUT;
	writeData = idAddress_v;
	setDataPinType(currentDataPinType, expectedDataPinType);
   }
  }
   break;
  case STATE_ID_ADDRESS_READ_1:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	debugDiodeSet();
	nextIndexLength = (TICKS_PER_BIT * BITS_SEND_WR) - 1;
	nextState = STATE_SUB_ADDRES_READ;
	expectedDataPinType = DIR_OUT;
	setDataPinType(currentDataPinType, expectedDataPinType);
	writeData = SCCB_ID_WR;
   }
  }
   break;
  case STATE_ID_ADDRESS_READ_2:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	debugDiodeSet();
	nextIndexLength = (TICKS_PER_BIT * BITS_SEND_WR) - 1;
	nextState = STATE_READ_DATA;
	expectedDataPinType = DIR_OUT;
	setDataPinType(currentDataPinType, expectedDataPinType);
	writeData = SCCB_ID_RD;
   }
  }
   break;
  case STATE_SUB_ADDRES_WRITE:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	cnt = 0;
	debugDiodeSet();
	stateMachineData.index = 0;
	nextIndexLength = (TICKS_PER_BIT * BITS_SEND_WR) - 1;
	nextState = STATE_WRITE_DATA;
	expectedDataPinType = DIR_OUT;
	writeData = subAddress_v;
	setDataPinType(currentDataPinType, expectedDataPinType);
   }
  }
   break;
  case STATE_SUB_ADDRES_READ:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	readSequence = true;
	debugDiodeSet();
	nextIndexLength = 3;
	nextState = STATE_STOP;
	expectedDataPinType = DIR_OUT;
	setDataPinType(currentDataPinType, expectedDataPinType);
	writeData = subAddress_v;
   }
  }
   break;
  case STATE_READ_DATA:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	debugDiodeSet();
	nextIndexLength = 3;
	nextState = STATE_STOP;
	expectedDataPinType = DIR_IN;
	setDataPinType(currentDataPinType, expectedDataPinType);
	readSequence = false;
   }
  }
   break;
  case STATE_WRITE_DATA:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	cnt = 0;
	debugDiodeSet();
	stateMachineData.index = 0;
	nextIndexLength = 3;
	nextState = STATE_STOP;
	expectedDataPinType = DIR_OUT;
	writeData = data_v;
	setDataPinType(currentDataPinType, expectedDataPinType);
   }
  }
   break;
  case STATE_STOP:
  {
   if(stateMachineData.index == 0)		// provide next state constraints
   {
	cnt = 0;
	debugDiodeSet();
	stateMachineData.index = 0;
	nextIndexLength = (readSequence) ? 3 : 0;
	nextState = (readSequence) ? STATE_START_READ : STATE_FINISHED;
	expectedDataPinType = DIR_OUT;
	setDataPinType(currentDataPinType, expectedDataPinType);
   }
  }
   break;

  case STATE_FINISHED:
  {
   while(1)
   {
	__asm("");
   }
  }

   break;

  default:
   break;
 }

 if(stateMachineData.state == STATE_START_WRITE || stateMachineData.state == STATE_START_READ)
 {
  startSequence();
 }
 else if(stateMachineData.state == STATE_STOP)
 {
  stopSequence();
 }
 else
 {
  if(stateMachineData.indexLength > stateMachineData.index)
  {
   if(stateMachineData.index % 2 == 1)
   {
	changeClkState(clkState);
   }
   else if(((stateMachineData.index % TICKS_PER_BIT) == 0))
   {
	if(stateMachineData.state != STATE_READ_DATA)
	{
	 assert(clkState == IS_LOW);
	 if(stateMachineData.index / TICKS_PER_BIT < 8)
	 {
	  dataWrite(writeData, (stateMachineData.index / TICKS_PER_BIT));
	 }
	 else
	 {
	  expectedDataPinType = DIR_IN;
	  setDataPinType(currentDataPinType, expectedDataPinType);
	 }
	}
	else if(stateMachineData.state == STATE_READ_DATA)
	{
	 static uint8_t data = 0x0;

	 assert(clkState == IS_LOW);
	 if(stateMachineData.index / TICKS_PER_BIT < 8)
	 {
	  readData(&data, (stateMachineData.index / TICKS_PER_BIT));
	 }
	 else
	 {
	  dataReturn = data;
	  assert(clkState == IS_LOW);
	  expectedDataPinType = DIR_OUT;
	  setDataPinType(currentDataPinType, expectedDataPinType);
	  dataEdgeRising();
	 }
	}
   }

   stateMachineData.index++;
  }
  else
  {
   changeClkState(clkState);
   stateMachineData.index = 0;
   stateMachineData.indexLength = nextIndexLength;
   stateMachineData.state = nextState;
   expectedDataPinType = DIR_OUT;
   debugDiodeReset();
   cnt = 0;
  }
 }
}
