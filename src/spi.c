/*
 * spi.c
 *
 *  Created on: Dec 20, 2020
 *      Author: MiM
 */
#include "spi.h"

#define SPI5_SCK_PORT	GPIOF
#define SPI5_SCK_PIN	7
#define SPI5_MOSI_PORT	GPIOF
#define SPI5_MOSI_PIN	9

static inline void spiGpioInit(void)
{
 gpioPinConfiguration(SPI5_SCK_PORT, SPI5_SCK_PIN,
	                  GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
 gpioPinAlternateFunctionConfiguration(SPI5_SCK_PORT, SPI5_SCK_PIN, 5);
 gpioPinConfiguration(SPI5_MOSI_PORT, SPI5_MOSI_PIN,
	                  GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
 gpioPinAlternateFunctionConfiguration(SPI5_MOSI_PORT, SPI5_MOSI_PIN, 5);
}

void spiInit(void)
{
 RCC->APB2ENR |= RCC_APB2ENR_SPI5EN;
 __DSB();

 spiGpioInit();

 SPI5->CR2 |= SPI_CR2_TXEIE | SPI_CR2_TXDMAEN;
 SPI5->CR1 |= SPI_CR1_MSTR | SPI_CR1_SSI | SPI_CR1_SSM | SPI_CR1_SPE;

//  NVIC_EnableIRQ(SPI5_IRQn);

}

//void spiDmaInit(void)
//{
// DMA2_Stream6->PAR = (uint32_t)&SPI5->DR;
// DMA2_Stream6->M0AR = (uint32_t)frameBuffer;
// DMA2_Stream6->NDTR = 320*240;
// DMA2_Stream6->CR |= DMA_SxCR_CHSEL | DMA_SxCR_PL | DMA_SxCR_TCIE | DMA_SxCR_DIR_0 | DMA_SxCR_PSIZE_1 | DMA_SxCR_MSIZE_1;
// DMA2_Stream6->CR |= DMA_SxCR_ACK;
//
//}

void spiLcd8Bit(void)
{
 SPI5->CR1 &= ~(SPI_CR1_SPE);
 SPI5->CR1 &= ~(SPI_CR1_DFF);
 SPI5->CR1 |= SPI_CR1_SPE;
}

void spiLcd16Bit(void)
{
 SPI5->CR1 &= ~(SPI_CR1_SPE);
 SPI5->CR1 |= SPI_CR1_DFF;
 SPI5->CR1 |= SPI_CR1_SPE;
}

//void spiSend(uint16_t data)
//{
// while(!(SPI5->SR & SPI_SR_TXE)) {};
// SPI5->DR = data;
// while(!(SPI5->SR & SPI_SR_RXNE)) {};
// SPI5->DR;
//}

void spiSend(uint16_t data)
{
 while(!(SPI5->SR & SPI_SR_TXE)) {};
 SPI5->DR = data;
 while(!(SPI5->SR & SPI_SR_RXNE)) {};
 SPI5->DR;
}

void spiSendBuffer(uint8_t* data, uint8_t length)
{
 for(uint8_t i = 0; i < length; i++)
 {
  spiSend((uint8_t) (*(data + i)));
 }
}
