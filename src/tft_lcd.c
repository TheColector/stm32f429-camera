/*
 * lcdILI9341.c
 *
 *  Created on: Apr 22,  2020
 *      Author: MiM
 */

#include "tft_lcd.h"
#include <stdio.h>

//************************************************** LCD COMMANDS BEGIN ***********************************************

#define LCD_SOFT_RST   				0x01
#define LCD_EXIT_SLEEP 				0x11
#define LCD_NORMAL_MODE				0x13
#define LCD_DISP_OFF 				0x28
#define LCD_DISP_ON	 				0x29
#define LCD_PWR_CTR1 				0xC0
#define LCD_PWR_CTR2				0xC1
#define LCD_VCOM_CTR1 				0xC5
#define LCD_VCOM_CTR2				0xC7
#define LCD_MAC 					0x36
#define LCD_VERT_SCRL_START_ADDR	0x37
#define LCD_PIXEL_FORMAT_SET		0x3A
#define LCD_FRAME_RATE_CTR		 	0xB1
#define LCD_DISP_FUNC_CTR			0xB6
#define LCD_GAMMA_SET				0x26
#define LCD_POSTV_GAMMA_SET			0xE0
#define LCD_NEGTV_GAMMA_SET		 	0xE1
#define LCD_MEM_WRITE				0x2C

//************************************************** LCD GPIO PINS BEGIN *************************************************

#define LCD_CS_GPIO_PORT	GPIOC
#define LCD_CS_GPIO_PIN		2
#define LCD_DC_GPIO_PORT	GPIOD
#define LCD_DC_GPIO_PIN		13

#define LCD_WR_GPIO_PORT	GPIOF
#define LCD_WR_GPIO_PIN		7

#define LCD_DB10_PORT	GPIOC
#define LCD_DB10_PIN	7
#define LCD_DB11_PORT	GPIOD
#define LCD_DB11_PIN	3
#define LCD_DB12_PORT	GPIOC
#define LCD_DB12_PIN	10
#define LCD_DB13_PORT	GPIOB
#define LCD_DB13_PIN	0
#define LCD_DB14_PORT	GPIOA
#define LCD_DB14_PIN	11
#define LCD_DB15_PORT	GPIOA
#define LCD_DB15_PIN	12
#define LCD_DB16_PORT	GPIOB
#define LCD_DB16_PIN	1
#define LCD_DB17_PORT	GPIOG
#define LCD_DB17_PIN	6


//************************************************** LCD GPIO PINS END *************************************************

//************************************************** TYPEDEFS  BEGIN **************************************************

typedef enum
{
 DC_COMMAND,
 DC_DATA
} DataCommand;

typedef enum
{
 MODE_COLOR,
 MODE_DATA
} Mode;

//************************************************** TYPEDEFS END *****************************************************

//************************************************** STATICS  BEGIN ***************************************************

//static uint16_t frameBuffer[FRAME_HEIGHT*FRAME_WIDTH];

static inline void sendCmd(uint8_t cmd);

static void swapParameteres(Int* val1, Int* val2)
{
 Int tmp = *val1;
 *val1 = *val2;
 *val2 = tmp;
}

inline static void csEnable(bool state)
{
 if(state)
  LCD_CS_GPIO_PORT->BSRR |= GPIO_BSRR_BR2;		//CS goes to 0
 else
  LCD_CS_GPIO_PORT->BSRR |= GPIO_BSRR_BS2;		//CS goes to 1
}

inline static void dcMode(DataCommand dc)
{
 if(dc == DC_COMMAND)
  LCD_DC_GPIO_PORT->BSRR |= GPIO_BSRR_BR13;		//DC goes to 0
 else
  LCD_DC_GPIO_PORT->BSRR |= GPIO_BSRR_BS13;		//DC goes to 1
}

inline static void setMode(Mode mode)
{
 if(mode == MODE_COLOR)
 {
  sendCmd(LCD_MEM_WRITE);
  spiLcd16Bit();
 }
 else
  spiLcd8Bit();
}

static inline void sendCmd(uint8_t cmd)
{
 dcMode(DC_COMMAND);
 spiSend(cmd);
}

inline static void send(uint16_t data)
{
 dcMode(DC_DATA);
 spiSend(data);
}

//************************************************** STATICS  END *****************************************************

//************************************************** LCD INIT BEGIN ***************************************************

static inline void lcdGpioInit(void)
{
 gpioPinConfiguration(LCD_DC_GPIO_PORT, LCD_DC_GPIO_PIN,
	                  GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);
 gpioPinConfiguration(LCD_CS_GPIO_PORT, LCD_CS_GPIO_PIN,
	                  GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);
 gpioPinConfiguration(GPIOG, 13, GPIO_MODE_OUTPUT_PUSH_PULL_VERY_HIGH_SPEED_PULL_UP);
}

void lcdInit(void)
{
 lcdGpioInit();
 spiInit();
 csEnable(true);
 setMode(MODE_DATA);
 sendCmd(LCD_SOFT_RST);
 sendCmd(LCD_DISP_OFF);

 sendCmd(LCD_PWR_CTR1);
 send(0x23);

 sendCmd(LCD_PWR_CTR2);
 send(0x11);

 sendCmd(LCD_VCOM_CTR1);
 send(0x3E);
 send(0x28);

 sendCmd(LCD_VCOM_CTR2);
 send(0x86);

 sendCmd(LCD_MAC);
 send(0xE8);

 sendCmd(LCD_VERT_SCRL_START_ADDR);
 send(0x00);

 sendCmd(LCD_PIXEL_FORMAT_SET);
 send(0x55);

 sendCmd(LCD_FRAME_RATE_CTR);
 send(0x00);
 send(0x1B);

 sendCmd(LCD_DISP_FUNC_CTR);
 send(0x08);
 send(0x82);
 send(0x27);

 sendCmd(LCD_GAMMA_SET);
 send(0x01);

 sendCmd(LCD_POSTV_GAMMA_SET);
 send(0x0F);
 send(0x31);
 send(0x2B);
 send(0x0C);
 send(0x0E);
 send(0x08);
 send(0x4E);
 send(0xF1);
 send(0x37);
 send(0x07);
 send(0x10);
 send(0x03);
 send(0x0E);
 send(0x09);
 send(0x00);

 sendCmd(LCD_NEGTV_GAMMA_SET);
 send(0x00);
 send(0x0E);
 send(0x14);
 send(0x03);
 send(0x11);
 send(0x07);
 send(0x31);
 send(0xC1);
 send(0x48);
 send(0x08);
 send(0x0F);
 send(0x0C);
 send(0x31);
 send(0x36);
 send(0x0F);

 sendCmd(LCD_EXIT_SLEEP);

 sendCmd(LCD_DISP_ON);

 sendCmd(LCD_NORMAL_MODE);

 lcdSetOrientation(LCD_ORIENTATION_LANDSCAPE);
}

//************************************************** LCD INIT END *****************************************************

//************************************************** LCD DRAWING FUNCTIONS BEGIN **************************************

void lcdSetOrientation(LcdOrientation orientation)
{
	sendCmd(LCD_MAC);
	if (orientation == LCD_ORIENTATION_PORTRAIT_REV)		send(0x58);
	else if (orientation == LCD_ORIENTATION_PORTRAIT)		send(0x88);
	else if (orientation == LCD_ORIENTATION_LANDSCAPE_REV)	send(0x28);
	else if (orientation == LCD_ORIENTATION_LANDSCAPE)		send(0xE8);
}

void setDrawingWindow(Int xBegin, Int xEnd, Int yBegin, Int yEnd)
{
 uint8_t frame[4];

 frame[0] = xBegin >> 8;
 frame[1] = xBegin & 0xff;
 frame[2] = xEnd >> 8;
 frame[3] = xEnd & 0xff;

 sendCmd(0x2a);
 send(xBegin >> 8);
 send(xBegin & 0xff);
 send(xEnd >> 8);
 send(xEnd & 0xff);
// spiSendBuffer(frame, sizeof(frame));

 frame[0] = yBegin >> 8;
 frame[1] = yBegin & 0xff;
 frame[2] = yEnd >> 8;
 frame[3] = yEnd & 0xff;

 sendCmd(0x2b);
 send(yBegin >> 8);
 send(yBegin & 0xff);
 send(yEnd >> 8);
 send(yEnd & 0xff);
// spiSendBuffer(frame, sizeof(frame));
}

void lcdUpdate(void)
{

}
void preFill(void)
{

}

void lcdFill(Color color)
{

 gpioBitSet(GPIOG, 13);
 setMode(MODE_DATA);
 setDrawingWindow(0, LCD_HEIGHT - 1, 0, LCD_WIDTH - 1);
 setMode(MODE_COLOR);
 dcMode(DC_DATA);

 for(Int position = 0; position < LCD_HEIGHT * LCD_WIDTH; position++)
 {
  spiSend(color);
 }

// gpioBitReset(GPIOG, 13);
}

void lcdDrawImage(const uint16_t image[LCD_PIX_SIZE])
{
 setMode(MODE_DATA);
 setDrawingWindow(0, LCD_HEIGHT - 1, 0, LCD_WIDTH - 1);
 setMode(MODE_COLOR);
 dcMode(DC_DATA);

 for(uint32_t i = 0; i < LCD_HEIGHT*LCD_WIDTH; i++)
 {
  send(image[i]);
 }
}

void lcdDrawPixel(Int x, Int y, Color color)
{
 if(x < 0 || y < 0 || x >= LCD_WIDTH || y >= LCD_HEIGHT)
 {
  return;
 }

 setMode(MODE_DATA);
 setDrawingWindow(x, x, y, y);
 setMode(MODE_COLOR);
 send(color);
}

void lcdDrawHLine(Int x1, Int x2, Int y, Color color)
{
 for(Int positionX = x1; positionX <= x2; positionX++)
 {
  lcdDrawPixel(positionX, y, color);
 }
}

void lcdDrawVLine(Int x, Int y1, Int y2, Color color)
{
 for(Int positionY = y1; positionY <= y2; positionY++)
 {
  lcdDrawPixel(x, positionY, color);
 }
}

void lcdDrawLine(Int x1, Int y1, Int x2, Int y2, Color color)
{
//	Int positionY = 0;
//	Int coeff = 0;
//	if((y2-y1) >= (x2-x1))
//	{
//		coeff = ((y2-y1)*100000)/(x2-x1);
//	}
//	else
//	{
//		coeff = ((y2-y1)*100000)/(x2-x1);
//	}
//
//	for(Int positionX = x1; positionX <= x2; positionX++)
//	{
//		positionY = (positionX * coeff)/100000;
//		lcdDrawPixel(positionX, positionY, color);
//	}
//	if(x1 > x2)
//		swapParameteres(&x1, &x2);
//	if(y1 > y2)
//		swapParameteres(&y1, &y2);

 Int distX = x2 - x1;
 Int distY = y2 - y1;
 Int xSign = 1;
 Int ySign = 1;
 Int error;

 if(distX < 0)
 {
  xSign = -1;
 }
 else if(distX == 0)
 {
  lcdDrawVLine(x1, y1, y2, color);
  return;
 }

 if(distY < 0)
 {
//		swap(&y1, &y2);
  distY *= -1;
  ySign = -1;
 }
 else if(distY == 0)
 {
  lcdDrawHLine(x1, x2, y1, color);
  return;
 }

 if(distY < distX)
 {
  Int currentY = 0;

  error = (distY << 1) - distX;
  currentY = y1;

  for(Int i = x1; i <= x2; i++)
  {
   lcdDrawPixel(i, currentY, color);
   if(error > 0)
   {
	currentY += ySign;
	error -= (distX << 1);
   }
   error += (distY << 1);
  }
 }
 else
 {
  Int currentX = 0;

  error = (distX << 1) - distY;
  currentX = x1;

  for(Int i = y1; i <= y2; i++)
  {
   lcdDrawPixel(currentX, i, color);
   if(error > 0)
   {
	currentX += xSign;
	error -= (distY << 1);
   }

   error += (distX << 1);
  }
 }
}

void lcdDrawRectangle(Int x1, Int x2, Int y1, Int y2, Color color)
{
 for(Int positionX = x1; positionX <= x2; positionX++)
 {
  for(Int positionY = y1; positionY <= y2; positionY++)
  {
   lcdDrawPixel(positionX, positionY, color);
  }
 }
}

void lcdDrawCircle(Int x, Int y, Int r, Color color)
{
 for(Int positionX = x - r; positionX <= x + r; positionX++)
 {
  for(Int positionY = y - r; positionY <= y + r; positionY++)
  {
   Int lenX = x - positionX;
   Int lenY = y - positionY;
   Int lenXp = positionX - x;
   Int lenYp = positionY - y;

   if((lenX * lenX) + (lenY * lenY) < (r * r)
	|| (lenXp * lenXp) + (lenYp * lenYp) < (r * r))
	lcdDrawPixel(positionX, positionY, color);
  }
 }
}

//************************************************** LCD DRAWING FUNCTIONS END ****************************************

