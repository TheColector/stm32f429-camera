/*
 * timer.c
 *
 *  Created on: Nov 5, 2020
 *      Author: Michał Młodecki
 */
#include "stm32f429xx.h"
#include "system_init.h"

#define TIMER_INSTANCE	(TIM2)
#define TIMER_CNT		(TIMER_INSTANCE->CNT)
#define U_SECONDS		(1000000U)

void timerUsInit(void)
{
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

    TIMER_INSTANCE->PSC = (APB1_TIM_CLK_HZ/U_SECONDS) - 1;
    TIMER_INSTANCE->ARR = UINT32_MAX;

    TIMER_INSTANCE->CR1 = TIM_CR1_URS;
}

void timerUsStart()
{
 TIMER_CNT = 0;
 TIMER_INSTANCE->CR1 |= TIM_CR1_CEN;
}

void timerUsStop()
{
 TIMER_INSTANCE->CR1 &= ~(TIM_CR1_CEN);
 TIMER_CNT = 0;
}

void resetUsTime()
{
 TIMER_CNT = 0;
}

uint32_t getCurrentTimeUs()
{
 return TIMER_CNT;
}

void delayUs(uint32_t ms)
{
  uint32_t currentTime = TIMER_CNT;
  while(currentTime + ms > TIMER_CNT);
}

